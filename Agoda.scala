import java.io.{File, InputStream}
import java.nio.file.Files

/**
  * Created by bhingoleramdas143@gmail.com on 2/5/2017.
  */

object Agoda {

  def main(args: Array[String]): Unit = {
    val arbitaryNestedList = List(1, List(2, 3), List(List(List(List(4)), List(5)), List(6, 7)), 8)
    //Printing flatted list
    // function definition - flattenArbitoryList(arbitrary list)

    println(flattenArbitoryList(arbitaryNestedList))
    // Download files, stored on server and storing to location provided by us
    //Function definition - fileDownloader(listOfUrls,directoryNameToStore)
    fileDownloader(List("https://code.jquery.com/jquery-3.1.1.js","https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"))
  }

  /*Problem Statement 1
  * Write some code which will flatten an array of arbitrarily nested arrays of integers
  * into a flat array of integers. e.g. [[1,2,[3]],4] -> [1,2,3,4]
  * */
  def flattenArbitoryList(arbitoryList: List[Any]): List[Any] = arbitoryList flatMap {
    case nestedList: List[_] => flattenArbitoryList(nestedList)
    case elements => List(elements)
  }
/*  Problem Statement2 -
  Write a program that can be used to download data from multiple sources and
  protocols to local disk. The list of sources will be given as input in the form of urls
  (e.g. http://my.file.com/file, ftp://other.file.com/other, sftp://and.also.this/ending etc)
  . The program should download all the sources, to a configurable location
  (file name should be uniquely determined from the url) and then exit. In your code, please
  consider: The program should extensible to support different protocols, some sources might
  very big (more than memory), some sources might be very slow, while others might be fast,
  some sources might fail in the middle of download, we never want to have partial data in
  the final location.*/

  private def deleteFile(files: List[File]) =  {
    files.foreach { file => file.delete()}
  }

  def fileDownloader(urls: List[String], directoryToStoreFile: String = "") = {
    import java.net.URL
    import java.io.File
    var files: scala.collection.immutable.List[File] = scala.collection.immutable.List[File]()
    urls.foreach( url =>
      try {
        val in = new URL(url).openStream()
        val file: File = new File(buildFilePathToStore(url,directoryToStoreFile))
        writeToFile(in, file)
        files = file :: files
      }catch {
        case e: Exception => {
          deleteFile(files)
          println("Exception occurred so deleted partially download files")
        }
      }
    )
  }

  private def writeToFile(in: InputStream, file: File):Unit = Files.copy(in, file.toPath())

  private def buildFilePathToStore(urlToNameFile: String,directoryToStoreFile :String) :String ={
    val workingDir = System.getProperty("user.dir")
    val fileName = urlToNameFile.replace("/", "").replace(":", "")
    workingDir + File.separator +directoryToStoreFile +File.separator + fileName
  }
}